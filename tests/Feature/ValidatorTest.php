<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ValidatorTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testControl()
    {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "Nat-han the man",
            "family_name" => "Rad'avanh",
            "licence_number" => "1234567890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("OK");
    }

    public function testNameTooLong() {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "NathanMaximusRadsavanh",
            "family_name" => "Radsavanh",
            "licence_number" => "1234567890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }

    public function testNameTooShort() {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "",
            "family_name" => "Radsavanh",
            "licence_number" => "1234567890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }

    public function testLongDriverLicense() {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "Nathan",
            "family_name" => "Radsavanh",
            "licence_number" => "12345612345677890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }

    public function testShortDriverLicense() {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "Nathan",
            "family_name" => "Radsavanh",
            "licence_number" => ""
        );
        $response = $this->json('POST', '/api/id', $arr);
        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }

    public function testBadCharGivenName()
    {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "Nath>an",
            "family_name" => "Radsavanh",
            "licence_number" => "1234567890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }

    public function testBadCharFamilyName()
    {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "Nathan",
            "family_name" => "Ra?savanh",
            "licence_number" => "1234567890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }

    public function testBadCharLicenceNum()
    {
        $arr = array(
            "type" => "driver_licence",
            "given_name" => "Nathan",
            "family_name" => "Radsavanh",
            "licence_number" => "123'567890"
        );
        $response = $this->json('POST', '/api/id', $arr);

        $response->assertStatus(200);
        $response->assertSee("ERROR");
    }
}
