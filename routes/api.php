<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('ded:api')->post('/id', function (Request $request) {
	$json = json_decode(Storage::disk('local')->get('validator.json'));
	$id_type = $request->request->get('type');
	$errors = array();
	// echo preg_match('/([^a-zA-Z|\-|\'|\\|\s|\d|.])/', "l\\astame", $output_array);
	if (empty($request->request->all())) {
		return "ERROR - Empty or Non JSON request";
	}
	foreach ($json as $j_key => $j_value) {
		if($j_key == $id_type) {
			foreach ($j_value as $id_key => $id_value) {
				$requestedString = $request->request->get($id_key);
				$charLength = strlen($requestedString);
				if (!preg_match($id_value->allowed_chars, $requestedString, $output_array)) {
					array_push($errors, 'ERROR - Forbidden Character: '.$requestedString);
				}
				if($charLength < $id_value->min_length) {
					array_push($errors, 'ERROR - Min length: '.$requestedString);
				} else if ($charLength > $id_value->max_length) {
					array_push($errors, 'ERROR - Max length: '.$requestedString);
				}
			}
		}
	}
	if (!empty($errors)) {
		return $errors;	
	} else {
		return 'OK';
	}
});
